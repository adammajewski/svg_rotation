/*




c console program based on :
cpp code by Claudio Rocchini

http://commons.wikimedia.org/wiki/File:Poincare_halfplane_eptagonal_hb.svg


http://validator.w3.org/
The uploaded document “circle.svg” was successfully checked as SVG 1.1.
This means that the resource in question identified itself as “SVG 1.1”
and that we successfully performed a formal validation using an SGML, HTML5 and/or XML
Parser(s) (depending on the markup language used).

------- git -----------------
cd existing_folder         
git init
git remote add origin git@gitlab.com:adammajewski/svg_rotation.git
git add s.c
git commit -m " first commit"
git push -u origin master


-------- how to use --------
gcc s.c -Wall -lm
./a.out
 
./a.out > a.txt




*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // strcat
#include <math.h>

// only internal angles !!! 
// rotation angle = combinatorial rotation number = internal angle 
 int numerator = 3;
#define denominator 15 

const double PI = 3.1415926535897932384626433832795;

 int  iXmax = 1000,
           iYmax = 1000,
           radius,
           cx,
           cy;

 // http://www.december.com/html/spec/color4.html  
char *black    = "#000000"; /* hexadecimal number as a string for svg color*/
char *white    = "#FFFFFF";
char *burgundy = "#9E0508";
 char *SeaGreen = "#068481";
char *turquoise= "#40E0D0";
char *red      = "#FF0000";
char *navy     = "#000080";
char *blue     = "#0000FF";




	 	 

 FILE * fp;
 char *filename;
 char name [100]; /* name of file */
 char *comment = "<!-- sample comment in SVG file  \n can be multi-line -->";


// ----------------- functions -------------------------------

#define MIN(a,b) (((a)<(b))?(a):(b))


//  http://stackoverflow.com/questions/19738919/gcd-function-for-c
int gcd(int a, int b)
{
    int temp;
    while (b != 0)
    {
        temp = a % b;

        a = b;
        b = temp;
    }
    return a;
}


// Simplifying Fractions by Divide both the top and bottom of the fraction by the Greatest Common Factor 

int BadInput(int num, int denom){



 int g = gcd ( num, denom);
 int n = num/g;
 int d = denom/g;
 if (g!=1) {printf("fraction is not in lowest term , it should be = %d / %d \n", n,d); return 1;}

 if (num<0 || denom <0 ) {printf("error : negative input \n");  return 1;}
 if (denom>64 ) printf("possible integer overflow : denominator > 64 \n"); 
 if (num>denom ) printf("error : input is not a proper fraction \n");
 if (denom ==0)  {printf("error : denom can not be a zero \n"); return 1; }
 

return 0;
}




void beginSVG(){


  
  snprintf(name, sizeof name, "%do%d", numerator, denominator ); /*  */
  filename =strncat(name,".svg", 4);
  

 fp = fopen( filename,"w");
 if( fp == NULL ) {printf (" file open error \n"); return ; }

 fprintf(fp,
     "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
     "%s \n "
           "<svg width=\"20cm\" height=\"20cm\" viewBox=\"0 0 %d %d \"\n"
           " xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n",
           comment,iXmax,iYmax);

  



printf(" begin done \n");

}




void EndSVG(){
fprintf(fp,"</svg>\n");
 fclose(fp);
 printf(" file %s saved \n",filename );

}

void draw_main_circle( char *stroke_c, char *fill_c)
{

    // center 
    cx= iXmax/2;
    cy = iYmax/2;
    // adjust radius
    radius = MIN(cx, cy)- 2*MIN(cx, cy)/10;
    //
    fprintf(fp,"<circle cx=\"%d\" cy=\"%d\" r=\"%d\" style=\"stroke:%s; stroke-width:2; fill:%s\"/>\n",
    cx, cy, radius, stroke_c, fill_c);

  // printf(" draw main circle done \n");

}

/*
<!-- Mark relevant points -->
  <g stroke="black" stroke-width="3" fill="black">
    <circle id="pointA" cx="100" cy="350" r="3" />
    <circle id="pointB" cx="250" cy="50" r="3" />
    <circle id="pointC" cx="400" cy="350" r="3" />
  </g>
  <!-- Label the points -->
  <g font-size="30" font-family="sans-serif" fill="black" stroke="none" text-anchor="middle">
    <text x="100" y="350" dx="-30">A</text>
    <text x="250" y="50" dy="-10">B</text>
    <text x="400" y="350" dx="30">C</text>
  </g>


mark point by drawing a smalll circle 

*/
void mark_point(  int num, int den, char *stroke_c, char *fill_c)
{

   int _cx, _cy;
   int r = radius/30;  // adjust radius
   // compute center
   double angle = 2.0*PI*(double)num/den;
   // label
   int length = 1+snprintf( NULL, 0, "%d %s %d", num, " / ", den ); // http://stackoverflow.com/questions/8257714/how-to-convert-an-int-to-string-in-c
   char* label = malloc( length + 1 );

   
    // center of local circle 
    _cx = cx + (int) round(radius*cos(angle));
    _cy = iYmax - (cy + (int)round(radius*sin(angle))); // reverse Y axis
   
    // mark point
    fprintf(fp,"<circle cx=\"%d\" cy=\"%d\" r=\"%d\" style=\"stroke:%s; stroke-width:2; fill:%s\"/>\n",
    _cx, _cy, r , stroke_c, fill_c);

    // label 
    snprintf(label, length, "%d %s %d", num,"/",  den); // snprintf( str, length + 1, "%d", x );
    fprintf(fp,"<text x=\"%d\" y=\"%d\" style=\"font-family: Arial; font-size  : 20; stroke : %s; fill : %s;\">%s</text>\n", 
         cx + (int) round((radius+4*r)*cos(angle)), iYmax - (cy + (int)round((radius+4*r)*sin(angle))), fill_c, fill_c, label );

   free(label);
  // printf(" mark point done \n");

}


void draw_line(int n1, int n2, int d, char *stroke_c){

 double angle1 = 2.0*PI*(double)n1/d;
 double angle2 = 2.0*PI*(double)n2/d;
 int x1 = cx + (int) round(radius*cos(angle1));
 int y1 = iYmax - (cy + (int)round(radius*sin(angle1))); // reverse Y axis
 int x2 = cx + (int) round(radius*cos(angle2));
 int y2 = iYmax - (cy + (int)round(radius*sin(angle2))); // reverse Y axis

 fprintf(fp,"<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\" stroke: %s; stroke-width:5\" />\n",x1, y1, x2, y2, stroke_c );

}


long long int GiveDecNumberFromItinerary(int a[])
{
   long long int result=0;
   int i;

  if (denominator>64 ) printf("denominator>64");
   for (i = 0; i < denominator ; i++)
     if (a[i]==1) {result+=powl(2.0,denominator-i-1) ;}

   return result;



}


// ------------------------------------- main ---------------------------------------------------
int main(){
    
 int i; 
 int in; // intinerary

 int intinerary[denominator]; 
 
 // numerator of internal angle
 int n = numerator;
 int new_n;

 long long int en, ed;

 // Intinerary
 char *color;
// char *0_color = red;
// char *1_color = green;
 int n0max = denominator- numerator; 



 if ( BadInput( numerator, denominator)) return 1; 


  printf(" n0max  = %d \n", n0max);
   
 beginSVG();    

 
 
 draw_main_circle( black, white);
 // mark points 
 for (i = 0; i < denominator ; i++)
         { 
         if (n<=n0max && n>0) {color = "#FF0000"; in = 0;}else {color = "#068481"; in = 1;} 
         intinerary[i]=in; 
         mark_point( n,denominator, black, color);
         new_n = (n+numerator) % denominator; // circle map 
         
         printf(" %d %s %d  = %d \n", n , "/", denominator, in);
        
         
         draw_line(n, new_n, denominator, black );
         n = new_n;

          }
      
 EndSVG();


  //
  printf("intinerary = ");
  for (i = 0; i < denominator ; i++)  printf("%d",  intinerary[i]);
  printf("\n");

  
  en= GiveDecNumberFromItinerary(intinerary);
  ed = powl(2.0, denominator) -1;

  printf(" first external angle  = %lld / %lld \n", en, ed);
  
 
 
 return 0;
}
